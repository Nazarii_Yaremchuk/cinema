package com.yaremchuk.cinema.repository;

import com.yaremchuk.cinema.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmRepo extends JpaRepository<Film, Long> {

}
