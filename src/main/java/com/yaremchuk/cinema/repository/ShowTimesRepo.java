package com.yaremchuk.cinema.repository;

import com.yaremchuk.cinema.model.ShowTimes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShowTimesRepo extends JpaRepository<ShowTimes, Long> {


}
