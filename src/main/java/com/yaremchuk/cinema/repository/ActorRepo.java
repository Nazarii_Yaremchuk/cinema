package com.yaremchuk.cinema.repository;

import com.yaremchuk.cinema.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorRepo extends JpaRepository<Actor, Long> {

}
