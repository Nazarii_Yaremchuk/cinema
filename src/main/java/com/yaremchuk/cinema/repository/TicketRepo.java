package com.yaremchuk.cinema.repository;

import com.yaremchuk.cinema.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepo extends JpaRepository<Ticket, Long> {

}
