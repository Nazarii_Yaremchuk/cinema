package com.yaremchuk.cinema.model;

public enum FilmGenre {
    ACTION,
    COMEDY,
    DRAMA,
    FANTASY,
    HORROR,
    MYSTERY,
    ROMANCE,
    THRILLER,
    WESTERN;
}
