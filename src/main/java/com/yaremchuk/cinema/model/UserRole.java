package com.yaremchuk.cinema.model;

public enum UserRole {
    ADMIN,
    USER;
}
