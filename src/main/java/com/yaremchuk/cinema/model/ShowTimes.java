package com.yaremchuk.cinema.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "show_times")
public class ShowTimes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "day_time", nullable = false)
    private Integer dayTime;

    @OneToMany(mappedBy = "showTime")
    private List<Ticket> tickets;

    @ManyToOne
    private Film film;
}
