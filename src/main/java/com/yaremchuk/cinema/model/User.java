package com.yaremchuk.cinema.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Length(min = 4, max = 20)
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Length(min = 4, max = 20)
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Length(max = 30)
    private String email;

    @NotNull
    @Length(max = 30)
    private String password;

    @Length(max = 10)
    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @NotNull
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @OneToMany(mappedBy = "buyer")
    private List<Ticket> tickets;
}
