package com.yaremchuk.cinema.model;

public enum Sex {
    MAN,
    WOMAN;
}
