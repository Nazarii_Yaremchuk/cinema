package com.yaremchuk.cinema.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@Table(name = "actors")
public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Length(min = 4, max = 20)
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Length(min = 4, max = 20)
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Length(min = 3, max = 20)
    private String nationality;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @ManyToMany
    private List<Film> films;
}
