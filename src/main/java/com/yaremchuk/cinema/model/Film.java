package com.yaremchuk.cinema.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@Table(name = "films")
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Length(max = 40)
    @Column(name = "title", nullable = false)
    private String title;

    @Length(max = 100)
    private String description;

    private Float duration;

    @NotNull
    @Enumerated(EnumType.STRING)
    private FilmGenre genre;

    private Float rating;

    @ManyToMany(mappedBy = "films")
    private List<Actor> actors;

    @OneToMany(mappedBy = "film")
    private List<ShowTimes> showTimes;
}