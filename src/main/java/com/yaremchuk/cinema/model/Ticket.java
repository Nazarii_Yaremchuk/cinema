package com.yaremchuk.cinema.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "row_place", nullable = false)
    private Integer rowPlace;

    @Column(name = "place_number", nullable = false)
    private Integer placeNumber;

    @ManyToOne
    private User buyer;

    @ManyToOne
    private ShowTimes showTime;
}
