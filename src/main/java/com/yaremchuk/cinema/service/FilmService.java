package com.yaremchuk.cinema.service;

import com.yaremchuk.cinema.repository.FilmRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FilmService {
    private FilmRepo filmRepo;
}
