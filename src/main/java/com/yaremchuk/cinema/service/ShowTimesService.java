package com.yaremchuk.cinema.service;

import com.yaremchuk.cinema.repository.ShowTimesRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShowTimesService {
    private ShowTimesRepo showTimesRepo;
}
