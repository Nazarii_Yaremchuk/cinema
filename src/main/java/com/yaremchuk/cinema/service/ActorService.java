package com.yaremchuk.cinema.service;

import com.yaremchuk.cinema.repository.ActorRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ActorService {
    private ActorRepo actorRepo;
}
